import React from "react";
import classes from './MyProjects.module.css';
import Project from "./Project.js";
import foodOrderPic from "../Pictures/FoodOrder.png";
import weatherAppPic from "../Pictures/WeatherApp.png";
import proteinCounterPic from "../Pictures/ProteinCounter.png";
import expenseTrackerPic from "../Pictures/ExpenseTracker.png";

const MyProjects = props => {

  return (
    <div >
      <h1>Projects</h1>
      <div className={classes.projects}>
        <Project name="Food Order" imageSrc={foodOrderPic} linkToProject="https://order-food-beta.vercel.app/"/>
        <Project name="Weather App" imageSrc={weatherAppPic} linkToProject="https://weatherapp-three-self.vercel.app/"/>
        <Project name="Protein Counter" imageSrc={proteinCounterPic} linkToProject="https://proteincounter.vercel.app/"/>
        <Project name="Expenses Tracker" imageSrc={expenseTrackerPic} linkToProject="https://bht-expenses-tracker.vercel.app/"/>
      </div>
    </div>
  );
}

export default MyProjects;
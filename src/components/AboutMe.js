import React from "react";
import classes from "./AboutMe.module.css";
import TechStat from "./TechStats";
import profilePic from "../Pictures/FotoPerfilSiteNoBG.png";
import gitPic from "../Pictures/git.png";
import cPic from "../Pictures/c.png";
import javaPic from "../Pictures/java.png";
import jsPic from "../Pictures/js.png";
import nodejsPic from "../Pictures/nodejs.png";
import pythonPic from "../Pictures/python.png";
import reactPic from "../Pictures/react.png";
import downloadPic from "../Pictures/download.png";
import myResume from "../Documents/Bruno_Trevisan_Resume.pdf"

// All icons I'm using are from https://icons8.com.br/

const AboutMe = () => {
  return (
    <div className={classes.aboutMe}>
      <img src={profilePic} className={classes.picture} alt="Profile"></img>
      <h1>About Me</h1>
      <div className={classes.description}>
        <p >I am a fullstack developer, graduated in Computer Engineering, with almost 4 years of experience working on a wide variety of projects.<br/>I am based in Curitiba, Brazil.</p>
      </div>
      <div className={classes.techDiv}>
        <h1>Technologies</h1>
        <div className={classes.iconsDiv}>
          <TechStat techName="React" imageSrc={reactPic} level={5}/>
          <TechStat techName="NodeJs" imageSrc={nodejsPic} level={5}/>
          <TechStat techName="JavaScript" imageSrc={jsPic} level={5}/>
          <TechStat techName="Git" imageSrc={gitPic} level={4}/>
          <TechStat techName="Python" imageSrc={pythonPic} level={3}/>
          <TechStat techName="Java" imageSrc={javaPic} level={3}/>
          <TechStat techName="C" imageSrc={cPic} level={3}/>
        </div>
      </div>
      <div className={classes.downloadResume}>
        <a href={myResume} download="Bruno_Trevisan_Resume.pdf">
          <button className={classes.button}>Download Resume </button>
        </a>
        
      </div>
    </div>
  );
};

export default AboutMe;

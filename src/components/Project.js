import React from "react";
import classes from "./MyProjects.module.css";

const Project = (props) => {

  return (
    <div className={classes.projectInfo}>
      <a href={props.linkToProject} target="_blank">
        <h2>{props.name}</h2>
        <img src={props.imageSrc} className={classes.projectImage} alt="Icon"></img>
      </a>
    </div>
  )
}

export default Project;
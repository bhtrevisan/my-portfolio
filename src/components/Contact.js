import React, {useState} from "react";
import classes from "./Contacts.module.css";
import ContactCard from "./ContactCard";
import emailPic from "../Pictures/email.png";
import linkedinPic from "../Pictures/linkedin.png";
import gitlabPic from "../Pictures/gitlab.png";


const Contacts = (props) => {

  const [copied, setCopied] = useState(false);

  const copyEmail = async () => {
    await navigator.clipboard.writeText('brunohtervisan@gmail.com');
    setCopied(true);
    setTimeout(() => {
      setCopied(false);
    }, 2000);
  }

  return (
    <div>
      <h1>Contacts</h1>
      <div className={classes.contacts}>
        <ContactCard contactName="E-mail" imageSrc={emailPic} emailClick={copyEmail}/>
        {copied && <span style={{color:"#FFFFFF"}}>E-mail Copied!</span>}
        <ContactCard contactName="LinkedIn" imageSrc={linkedinPic} linkToPage="https://www.linkedin.com/in/bhtrevisan/"/>
        <ContactCard contactName="Gitlab" imageSrc={gitlabPic} linkToPage="https://gitlab.com/bhtrevisan"/>        
      </div>
    </div>
  );
}

export default Contacts;
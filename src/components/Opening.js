import React from "react";
import classes from './Opening.module.css';

const Opening = props => {

    return (
        <div className={classes.opening}>
            <p className={classes.myName}>Hello, I am <span className={classes.name}>Bruno</span></p>
            <p className={classes.positionName}>Fullstack Developer</p>
        </div>
    );
}

export default Opening;
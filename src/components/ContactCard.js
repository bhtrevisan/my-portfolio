import React from "react";
import classes from "./Contacts.module.css";

const ContactCard = (props) => {

  return (
    <div>
      <a href={props.linkToPage} target="_blank"  rel="noreferrer" onClick={props.emailClick} className={classes.contactInfo}>
        <img src={props.imageSrc} alt="Contact link"></img>
        <span>{props.contactName}</span>
      </a>
    </div>
  );
}

export default ContactCard;
import React from "react";
import classes from "./AboutMe.module.css";

const TechStat = (props) => {

  return (
    <div className={classes.techStats}>
      <div className={classes.progressBar}>
        <div className={(props.level >= 1) ? classes.squareActive : classes.square}></div>
        <div className={(props.level >= 2) ? classes.squareActive : classes.square}></div>
        <div className={(props.level >= 3) ? classes.squareActive : classes.square}></div>
        <div className={(props.level >= 4) ? classes.squareActive : classes.square}></div>
        <div className={(props.level >= 5) ? classes.squareActive : classes.square}></div>
      </div>
      <img src={props.imageSrc} alt="Icon"></img>
      <p>{props.techName}</p>
    </div>
  );
}

export default TechStat;
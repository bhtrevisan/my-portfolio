import React from "react";
import classes from "./Footer.module.css";

const Footer = () => {
  const year = new Date().getFullYear();

  return (
    <footer className={classes.footer}>
      <div>
        {`© ${year} Bruno Trevisan`}
      </div>
    </footer>
  );
};

export default Footer;
import React from "react";
import Header from "./components/Header";
import Footer from "./components/Footer";
import Opening from "./components/Opening";
import AboutMe from "./components/AboutMe";
import MyProjects from "./components/MyProjects";
import Contacts from "./components/Contact";

import classes from "./App.css";

function App() {
  
  return (
    <React.Fragment>
      <Header/>
      <div>
        <Opening/>
      </div>
      <section>
        <AboutMe/>
      </section>
      <div>
        <MyProjects/>
      </div>
      <section>
        <Contacts/>
      </section>
      <Footer/>
    </React.Fragment>
  );
}

export default App;
